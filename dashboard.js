﻿
function initTable() {
	var initTableData = [
		{
			id: 1001,
			firstname: "Don Vito",
			lastname: "Corleone",
			year: "1920"
		},
		{
			id: 1002,
			firstname: "Lucy",
			lastname: "Mancini",
			year: "1941"
		},
		{
			id: 1003,
			firstname: "Jack",
			lastname: "Woltz",
			year: "1940"
		},
		{
			id: 1004,
			firstname: "Kay",
			lastname: "Adams",
			year: "1928"
		},
		{
			id: 1005,
			firstname: "Johnny",
			lastname: "Fontane",
			year: "1926"
		},
		{
			id: 1006,
			firstname: "Paulie",
			lastname: "Gatto",
			year: "1931"
		},
		{
			id: 1007,
			firstname: "Moe",
			lastname: "Greene",
			year: "1922"
		},
		{
			id: 1008,
			firstname: "Bruno",
			lastname: "Tattaglia",
			year: "1922"
		},
		{
			id: 1009,
			firstname: "Tom",
			lastname: "Hagen",
			year: "1932"
		}
	];
	
	for (var i=initTableData.length-1; i>0; i--) {
		var p = initTableData[i];
		var tr = $("<tr><td>"+p.id+"</td><td>"+p.firstname+"</td><td>"+p.lastname+"</td><td>"+p.year+"</td></tr>");
		$("#personDataTable tbody").append(tr);
	}
};

function onClickBtnCaclYoungest(ev) {
	var youngestIndex = -1;
	var youngestYear = -1;
	$("#personDataTable tbody tr").each(function(idx, elem){
		var cells = $(this).find("td");
		var year = parseInt($(cells[3]).text());
		if (year > youngestYear) {
			youngestYear = year;
			youngestIndex = idx;
		}
	});
	$($("#personDataTable tbody tr")[youngestIndex]).css('backgroundColor','red');
};

function onClickCrazyThing(ev) {
	var funcs = [];
	for (var i = 0; i < 3; i++) {
	   var fun = function() {
		   //console.log(i);
		   $($("#personDataTable tbody tr")[i]).css('backgroundColor','green');
	   };
	   funcs[i] = fun;
	}
	funcs.forEach(function(func) {
		func();
	});

};

$().ready(function() {
	initTable();
	
	$("#btnCalcYoungest").click(onClickBtnCaclYoungest);
	$("#btnMarkTheFirstThree").click(onClickCrazyThing);
	
});